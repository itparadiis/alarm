//��� ��������� � ST_CP ����� 74HC595
const int latchPin = 8;
//��� ��������� � SH_CP ����� 74HC595
const int clockPin = 12;
//��� ��������� � DS ����� 74HC595
const int dataPin = 11;
 
char inputString[2];
 
void setup() {
   //������������� ����� OUTPUT
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT); 
  pinMode(clockPin, OUTPUT);
  Serial.begin(9600);
  Serial.println("reset");
}
 
void loop() {
  // �������� ������ �� ���� 16 ������� ���� ���������
  for (int thisLed = 0; thisLed < 16; thisLed++) {
    // ���������� ������ � ������� ��� ���������� ����������
    registerWrite(thisLed, HIGH);
    // ���� ��� �� ������ ���������, �� ��������� ����������
    if (thisLed > 0) {
      registerWrite(thisLed - 1, LOW);
    }
    // ���� ��� ������ ���������, �� ��������� ���������
    else {
      registerWrite(15, LOW);
    }
    // ������ ����� ����� ���������� ���������
    delay(250);
  }
 
}
 
// ���� ����� �������� ��� �� ��������� �������
 
void registerWrite(int whichPin, int whichState) {
  // ��� �������� 16 ����� ���������� unsigned int
  unsigned int bitsToSend = 0;   
 
  // ��������� ���������� �� ����� �������� �����
  digitalWrite(latchPin, LOW);
 
  // ������������� HIGH � ��������������� ���
  bitWrite(bitsToSend, whichPin, whichState);
 
  // ��������� ���� 16 ��� �� ��� �����
  // ��� ������ � ������ � ������ �������
  byte registerOne = highByte(bitsToSend);
  byte registerTwo = lowByte(bitsToSend);
 
  // "������������" ����� � ��������
  shiftOut(dataPin, clockPin, MSBFIRST, registerTwo);
  shiftOut(dataPin, clockPin, MSBFIRST, registerOne);
 
  // "�����������" �������, ����� ���� ��������� �� ������� ��������
  digitalWrite(latchPin, HIGH);
}
