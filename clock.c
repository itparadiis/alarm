// инициализация начальных установок
void RTC_init(void){
	i2c_start_cond();               // запуск i2c
	i2c_send_byte(RTC_adr_write);   // передача адреса устройства, режим записи
	i2c_send_byte(0x0E);	        // передача адреса памяти
	i2c_send_byte(0b00100000);      // запустить преобразование температуры и выход на 1 Гц
	i2c_send_byte(0b00001000);      // разрешить выход 32 кГц
	i2c_stop_cond();                // остановка i2c
}

// получение времени и даты
void RTC_read_time(void){
	i2c_start_cond();               // запуск i2c
	i2c_send_byte(RTC_adr_write);   // передача адреса устройства, режим записи
	i2c_send_byte(0x00);	        // передача адреса памяти
	i2c_stop_cond();                // остановка i2c
	i2c_start_cond();               // запуск i2c
	i2c_send_byte(RTC_adr_read);    // передача адреса устройства, режим чтения
	sec = bcd(i2c_get_byte(0));     // чтение секунд, ACK
	min = bcd(i2c_get_byte(0));     // чтение минут, ACK
	hour = bcd(i2c_get_byte(0));    // чтение часов, ACK
	wday = bcd(i2c_get_byte(0));    // чтение день недели, ACK
	day = bcd(i2c_get_byte(0));     // чтение число, ACK
	month = bcd(i2c_get_byte(0));   // чтение месяц, ACK
	year = bcd(i2c_get_byte(1));    // чтение год, NACK
	i2c_stop_cond();                // остановка i2c
}

// установка времени
void RTC_write_time(unsigned char hour1,unsigned char min1, unsigned char sec1){
    i2c_start_cond();               // запуск i2c
    i2c_send_byte(RTC_adr_write);   // передача адреса устройства, режим записи
    i2c_send_byte(0x00);	        // передача адреса памяти
    i2c_send_byte(bin(sec1));       // 0x00 секунды (целесообразно ли задавать еще и секунды?)
  	i2c_send_byte(bin(min1));       // 0x01 минуты
    i2c_send_byte(bin(hour1));      // 0x02 часы
    i2c_stop_cond();                // остановка i2c
}

// установка даты
void RTC_write_date(unsigned char wday, unsigned char day, unsigned char month, unsigned char year){
    i2c_start_cond();               // запуск i2c
    i2c_send_byte(RTC_adr_write);   // передача адреса устройства, режим записи
    i2c_send_byte(0x03);	    // передача адреса памяти
    i2c_send_byte(bin(wday));       // 0x03 день недели (воскресенье - 1, пн 2, вт 3, ср 4, чт 5, пт 6, сб 7)
    i2c_send_byte(bin(day));        // 0x04 день месяц
	  i2c_send_byte(bin(month));      // 0x05 месяц
  	i2c_send_byte(bin(year));       // 0x06 год
    i2c_stop_cond();                // остановка i2c
}

// чтение температуры
void RTC_read_temper(void){
	i2c_start_cond();               // запуск i2c
	i2c_send_byte(RTC_adr_write);   // передача адреса устройства, режим записи
	i2c_send_byte(0x11);	          // передача адреса памяти
	i2c_stop_cond();                // остановка i2c
	i2c_start_cond();               // запуск i2c
	i2c_send_byte(RTC_adr_read);    // передача адреса устройства, режим чтения
	t1 = i2c_get_byte(0);           // чтение MSB температуры
	t2 = i2c_get_byte(1);           // чтение LSB температуры
	i2c_stop_cond();                // остановка i2c
	t2=(t2/128); 					          // сдвигаем на 6 - точность 0,25 (2 бита)
	t2=t2*5;                       // сдвигаем на 7 - точность 0,5 (1 бит)
}

//TODO:

